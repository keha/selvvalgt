# Læringsmål
## Viden:
 - Grunlæggende krypografi.
 - TLS's anvendelse af kryptografi.
## Færdigheder:
 - Kan anvende 3. parts implementering til kryptering.
 - Kan anvende TLS i praksis.
## Kompetencer:
 - Kan lave en distribueret opsætning som anvender kryptografi.