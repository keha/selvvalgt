# Projektmål

RSA:
Analyser implementeringsaspekterne af RSA-algoritmen og udvikle et praktisk system til sikker kommunikation baseret på RSA-kryptografi, inklusive nøglegenerering, kryptering og dekryptering.

TLS:
Evaluere sikkerhedsaspekterne af TLS-protokollen i forhold til sikkerhedstrusler og angreb, og udvikle retningslinjer og bedste praksis til konfiguration og implementering af TLS i webbaserede applikationer.

Praktisk implementering af kryptografiske algoritmer i software:
Udvikle en bibliotek af kryptografiske funktioner, der omfatter kryptering, dekryptering og hashfunktionsoperationer, og demonstrer deres brug i forskellige softwareapplikationer med fokus på sikkerhed og effektivitet.

Steganografi:
Udvikle og implementere en steganografisk metode til skjult kommunikation i digitale billeder og evaluere dens effektivitet og sikkerhed i forhold til deteksjon og skjulningskapacitet.
