Problemformulering:
I en digitaliseret verden, hvor informationsudveksling spiller en afgørende rolle, er sikkerhed af kritisk betydning for at beskytte data mod uautoriseret adgang, manipulation og tyveri. Denne opgave sigter mod at undersøge og udforske forskellige aspekter af kryptografi med fokus på steganografi, RSA, TLS og praktisk implementering af kryptografiske algoritmer i software. Gennem analytisk undersøgelse, praktisk eksperimentering og litteraturstudier vil opgaven adressere følgende spørgsmål:

 1. Hvordan fungerer RSA-algoritmen, og hvordan kan den anvendes til at sikre fortrolig kommunikation og digitale signaturer?
 2. Hvad er sikkerhedsaspekterne ved TLS-protokollen, og hvilke best practices bør overvejes ved implementering af TLS i webbaserede applikationer?
 3. Hvordan kan kryptografiske algoritmer implementeres i software, og hvilke udfordringer og bedste praksis er forbundet med praktisk implementering?
 4. Hvordan kan steganografi anvendes til at skjule data i digitale medier, og hvad er effektiviteten og sikkerhedsaspekterne ved forskellige steganografiske metoder?<br>

Ved at besvare disse spørgsmål vil opgaven bidrage til en dybere forståelse af kryptografi og dets anvendelser i sikkerhedsdomænet og identificere muligheder og udfordringer ved implementeringen af kryptografiske teknikker i softwaremiljøer.
