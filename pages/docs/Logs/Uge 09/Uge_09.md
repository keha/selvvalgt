# Uge 09

Jeg har bogen "Applied Cryptography Protocols, Algorithms And Source Code In C"
Derud over har jeg fundet bogen "Cryptography Engineering Design Principles And Practical Applications". Jeg har "bladret" lidt igennem begge og de har nogle spændende emner, men går også han over meget af det samme.

Jeg har kigget lidt på hvad der er på tryhackme <br>
Search: Encryption <br>
[encryption - crypto 101](https://tryhackme.com/room/encryptioncrypto101) (Medium) <br>
[Introduction to Cryptography](https://tryhackme.com/room/cryptographyintro) (Medium) <br>
[cyborg](https://tryhackme.com/room/cyborgt8) (Easy) <br>
[lunizz CTF](https://tryhackme.com/room/lunizzctfnd) (Medium) <br>
[Adventure Time (CTF)](https://tryhackme.com/room/adventuretime) (Hard) <br>


