# Uge 13

## Projekt status
Fremgang

## Ændringer i målene?

---

I denne uge fik jeg blandt andet lavet en projektplan i stil med hvad jeg lærte ved mit praktikforløb hos Innopixel på datamatikeruddannelsen. Da tiden allerede er løbet meget fra mig i dette projekt har jeg valg at tage udgangspunkt i mine læringsmål og bruge dem til at dele min projektplan op i faser, med underliggende aktiviteter. Dette skaber også mere et overblik for mig og giver mig en mere detaljeret tjekliste. 

Der ud over har jeg lavet forskellige rum på tryhackme for at få en overordnet generelt viden omkring kryptografi, RSA, TLS og AES. Jeg har set videoer omkring implementionen af caesar cipher, og jeg har implementeret det.

## Projektplan
**Fase 1: Introduktion til Grundlæggende Kryptografi** <br>
Aktivitet 1: Studiemateriale <br>
Læsning af grundlæggende kryptografi teori og principper. <br>
Videoer om grundlæggende kryptografi. <br>
Lave TryHackMe rum  <br>
Aktivitet 2: Praktiske øvelser <br>
Implementering af grundlæggende kryptografiske algoritmer som AES, RSA osv. <br>
Øvelser i kryptering og dekryptering af data. <br>

**Fase 2: Studie af TLS og dets anvendelse** <br>
Aktivitet 3: Forsknings- og studiemateriale <br>
Gennemgang af TLS-protokollen og dens anvendelse i sikkerheds kontekst. <br>
Videoer om grundlæggende kryptografi. <br>
Lave TryHackMe rum  <br>
Aktivitet 4: Praktisk eksperimentering <br>
Opsætning af en simpel TLS-server og klient. <br>
Gennemførelse af TLS-håndtryk og dataoverførsel mellem dem. <br>

**Fase 3: Anvendelse af 3. parts implementering til kryptering** <br>
Aktivitet 5: Evaluering af forskellige kryptografiske biblioteker <br>
Gennemgang af populære kryptografiske biblioteker som OpenSSL. <br>
Valg af det mest passende bibliotek. <br>
Aktivitet 6: Integration og implementering <br>
Integration af det valgte bibliotek i projektets kodebase. <br>
Implementering af kryptografiske funktioner ved hjælp af det valgte bibliotek. <br>

**Fase 4: Anvendelse af TLS i praksis** <br>
Aktivitet 7: Design af sikker kommunikationsprotokol <br>
Identifikation af kommunikationsbehov og sikkerhedsparametre. <br>
Design af en tilpasset protokol baseret på TLS til projektets formål. <br>
Aktivitet 8: Implementering <br>
Implementering af den tilpassede protokol med TLS-understøttelse. <br>

**Fase 5: Distribueret opsætning med kryptografi** <br>
Aktivitet 9: Arkitekturdesign <br>
Design af en distribueret systemarkitektur, der anvender kryptografi til sikker kommunikation og dataudveksling. <br>
Identifikation af relevante komponenter og deres rolle i systemet. <br>

**Fase 6: Evaluering og afslutning** <br>
Aktivitet 10: Dokumentation <br>
Udarbejdelse af detaljeret dokumentation. <br>
Aktivitet 11: Afrunding af projektet <br>
Sammenfattende rapport om projektets resultater, erfaringer og eventuelle udestående udfordringer. <br>

---



