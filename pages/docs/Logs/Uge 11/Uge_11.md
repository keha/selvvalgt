# Uge 11

## Projekt status
Jeg er Kommet lidt mere i gang! Jeg har undersøgt kortvarigt omkring de mange emner inden for kryptografi. Jeg har fået lave en godt udkast til min problemformulering og nogle projektmål. Jeg har stadig lidt svært ved at få opsat mine læringsmål..

## Ændringer i målene?



---

\- Da jeg har valg et meget bredt emne, kryptografi, har jeg lavet min problemformulering, projektmål og læringsmål på en lidt anderledes måde.
Jeg har valg at gå efter det overordende mål, som er: **Få så meget ny viden som muligt inden for krytologi.** <br>
Derfor har jeg lavet en liste af underemner jeg finder spændende, og jeg har med vilje lavet skopet for det her projekt alt for stort. Dette gør også at jeg så har meget svært ved at opstille direkte læringsmål.

---

## Problemformulering:

I en digitaliseret verden, hvor informationsudveksling spiller en afgørende rolle, er sikkerhed af kritisk betydning for at beskytte data mod uautoriseret adgang, manipulation og tyveri. Denne opgave sigter mod at undersøge og udforske forskellige aspekter af kryptografi med fokus på steganografi, RSA, TLS, hashfunktioner, biometrisk kryptografi og praktisk implementering af kryptografiske algoritmer i software. Gennem analytisk undersøgelse, praktisk eksperimentering og litteraturstudier vil opgaven adressere følgende spørgsmål:

1. Hvordan kan steganografi anvendes til at skjule data i digitale medier, og hvad er effektiviteten og sikkerhedsaspekterne ved forskellige steganografiske metoder?
2. Hvordan fungerer RSA-algoritmen, og hvordan kan den anvendes til at sikre fortrolig kommunikation og digitale signaturer?
3. Hvad er sikkerhedsaspekterne ved TLS-protokollen, og hvilke best practices bør overvejes ved implementering af TLS i webbaserede applikationer?
4. Hvad er egenskaberne og anvendelserne af kryptografiske hashfunktioner, og hvordan kan de beskytte dataintegritet?
5. Hvordan kan biometrisk kryptografi integreres med kryptografiske teknikker til at styrke autentifikation og adgangskontrol?
6. Hvordan kan kryptografiske algoritmer implementeres i software, og hvilke udfordringer og bedste praksis er forbundet med praktisk implementering?

Ved at besvare disse spørgsmål vil opgaven bidrage til en dybere forståelse af kryptografi og dets anvendelser i sikkerhedsdomænet og identificere muligheder og udfordringer ved implementeringen af kryptografiske teknikker i softwaremiljøer.

---

## Projektmål:

* Steganografi:<br>
Udvikle og implementere en steganografisk metode til skjult kommunikation i digitale billeder og evaluere dens effektivitet og sikkerhed i forhold til deteksjon og skjulningskapacitet.

* RSA:<br>
Analyser implementeringsaspekterne af RSA-algoritmen og udvikle et praktisk system til sikker kommunikation baseret på RSA-kryptografi, inklusive nøglegenerering, kryptering og dekryptering.

* TLS:<br>
Evaluere sikkerhedsaspekterne af TLS-protokollen i forhold til sikkerhedstrusler og angreb, og udvikle retningslinjer og bedste praksis til konfiguration og implementering af TLS i webbaserede applikationer.

* Hashfunktioner:<br>
Udvikle en sikker hashfunktionsimplementering og integrere den i en applikation til beskyttelse af dataintegritet, og udfør grundige sikkerhedsanalyser for at vurdere dens modstandsdygtighed mod forskellige angreb.

* Biometrisk kryptografi:<br>
Design og implementer et biometrisk autentifikationssystem baseret på fingeraftryksgenkendelse og kryptografiske teknikker, og evaluere dets effektivitet, nøjagtighed og sikkerhed.

* Praktisk implementering af kryptografiske algoritmer i software:<br>
Udvikle en bibliotek af kryptografiske funktioner, der omfatter kryptering, dekryptering og hashfunktionsoperationer, og demonstrer deres brug i forskellige softwareapplikationer med fokus på sikkerhed og effektivitet.
