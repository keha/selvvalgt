# Uge 06

### Denne uge er itroduktion til opgaven.

Vi har gennemgået Læringsmålene for opgaven. <br>
Vi fået tider til den obligatoriske vejledningsmøder (min er kl 09:00 i lige uger)
#### Dagsorden til møder: <br>
* Projekt status (Afsluttede opgaver, opnået læringsmål,dokumentation mm.) <br>
* Ændringer i målene? <br>
* Mål for de næste 14 dage

Derefter har vi gennemgået eksamensformen og indholdet til rapporten.

Resten af tiden er gået med overvejelser af emne med en lille brainstorm.

Jeg ved stadig ikke helt hvad jeg gerne vil gå ind i med denne opgave, men under brainstormen blev der nævnt SecDevOps som jeg måske vil kigge mere ind i.