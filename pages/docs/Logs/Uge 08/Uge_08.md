# Uge 08

## Første vejlednings samtale
Vejledningen er i denne omgang over Zoom! - men det gør mig nu ikke noget.

Jeg fik en god snak med Martin omkring emner og min lærings metode som ikke er super organiseret. <br>
På baggrund af dette har jeg valgt at gå med Kryptografi, som er et meget fascinerende men stort emne. Men da jeg er lidt kaotisk i min tilgang, har jeg fået lov til at have et s stort emne så kan jeg også hoppe lidt rundt hvor jeg vil, og så må jeg indsnævre det lidt hen af vejen. <br>

Martin foreslog nogle bøger som jeg måske kunne bruge: <br>
- Serious Cryptography <br>
- Applied Cryptography

Der ud over fik vi opstillet nogle mål for de næste 2 uger inden næste vejledning. <br>
- Projektmål. <br>
- Læringsmål. <br>
- Problemformulering.