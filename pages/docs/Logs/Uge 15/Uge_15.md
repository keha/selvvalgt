# Uge 15

## Projekt status

## Ændringer i målene?

## Mål for de næste 14 dage <br>
Set op og klargøring til rapport skrivning.

---

#### Indledning.
* Start med at præsentere din rapport og dens formål.
* Beskriv kort, hvad læseren kan forvente at finde i rapporten.

#### Overordnet beskrivelse af det valgte emne.
* Giv en indledende beskrivelse af det emne, du har valgt at arbejde med.
* Forklar baggrunden for valget af dette emne og dets relevans.

#### Beskrivelse af projektplan.
* Præsenter din projektplan og de trin, du har fulgt for at gennemføre projektet.
* Angiv tidsrammen, ressourcer og ansvarsfordeling i projektet.

#### Beskrivelse af den overordnet tilgang til læringsprocessen af emnet.
* Forklar din tilgang til at lære og arbejde med emnet.
* Diskuter de metoder, værktøjer eller strategier, du har brugt til at forstå og behandle emnet.

#### Refleksioner over projektet samt læringsprocessen.
* Giv refleksioner over projektet, herunder udfordringer, succeser og læring.
* Diskuter, hvad der gik godt, og hvad der kunne have været gjort anderledes.
* Identificer eventuelle vigtige erkendelser eller indsigt, du har opnået gennem processen.

#### En tydelig reference til Gitlab projekt (produktet).
* Angiv en klar reference til GitLab-projektet, hvor læseren kan finde det endelige produkt eller arbejdet.
* Giv eventuelt en kort beskrivelse af produktet eller projektet, der blev udviklet.

