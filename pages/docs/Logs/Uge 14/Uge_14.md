# Uge 14

## Projekt status
Påske "ferie"
## Ændringer i målene?



---


I denne ude er der sket meget. Jeg har igen lavet forskellige rum på tryhackme for at få en overordnet generelt viden omkring kryptografi, RSA, TLS og AES, de forskellige rum går hen over de samme emner men med forskellige fokuspunkter. Jeg har set videoer omkring TLS i helhed og hvordan det er delt op i mindre dele, derudover har jeg set videoer af implementionen af TLS i helhed, og self-signed certificates, og jeg har implementeret det med en server applikation og en klient applikation, og fået dem forbundet og viste de dele der indgår i et TLS håndtryk.

Derudover har jeg set rigtig mange videoer for at forklare hvordan RSA fungere og jeg har lavet en applikation til at kryptere og dekryptere data med RSA. Der er rigtig meget at finde omkring dette emne, men jeg vil også sige at det er en af de tungere emner, som man kan dykke med ned i og bruge rigtig meget tid på.

Jeg har også dykket ned i Diffie Hellman nøgleudveklins algoritmen, som i sig selv er meget smart og meget spændende. Dog har jeg ikke implementeret dette.