# Uge 12

## Projekt status
Fremgang

## Ændringer i målene?
Jeg har alt for mange ting jeg gerne vil lære noget om, og jeg tror ikke der er tid til det hele. Jeg har kortet problemformulering og projektmål lidt ned og så håber jeg at der stadig er tid nok til at nå det hele.

## Vejledning
I denne uge var der vejledningsmøde med Martin, det var et meget produktivt. Jeg fik vendt med Martin hvordan jeg havde meget svært ved at sætte mig ned og få lavet noget på trods af at jeg synes at det er et mega spændende emne. 

Vi fik også snakket om mine læringsmål, hvor jeg så det mere som et overordnet mål og ikke som dem vi får fra skolens side. <br>
Jeg har derfor opsat det som de læringsmål vi også ser fra skolen side, og indelt dem i viden færdigheder og kompetencer. <br>
Dette giver mig også lidt mere af en tjekliste jeg kan gå efter. 

### Mål for de næste 14 dage
- Projektplan
- Grundlæggende kryptografi
- TLS's anvendelse af kryptografi


---

## Læringsmål

### Viden: <br>
 - Grunlæggende krypografi. 
 - TLS's anvendelse af kryptografi.

### Færdigheder:
 - Kan anvende 3. parts implementering til kryptering.
 - Kan anvende TLS i praksis.

### Kompetencer:
 - Kan lave en distribueret opsætning som anvender kryptografi.


## Problemformulering:

I en stadig stigende digitaliseret verden, hvor information bliver udvekslet mere end nogensinde, er sikkerhed af kritisk betydning for at beskytte data mod uautoriseret adgang, manipulation og tyveri. Denne opgave sigter mod at undersøge og udforske forskellige aspekter af kryptografi med fokus på TLS, RSA, steganografi og praktisk implementering af kryptografiske algoritmer i software. <br>
Gennem analytisk informations søgen, praktiske opgaver vil jeg i denne opgave forsøge at besvare følgende spørgsmål:

1. Hvordan kan steganografi anvendes til at skjule data i digitale medier, og hvad er effektiviteten og sikkerhedsaspekterne ved forskellige steganografiske metoder?
2. Hvordan fungerer RSA-algoritmen, og hvordan kan den anvendes til at sikre fortrolig kommunikation og digitale signaturer?
3. Hvad er sikkerhedsaspekterne ved TLS-protokollen, og hvilke best practices bør overvejes ved implementering af TLS i webbaserede applikationer?
4. Hvordan kan kryptografiske algoritmer implementeres i software, og hvilke udfordringer og bedste praksis er forbundet med praktisk implementering?

Ved at besvare disse spørgsmål vil opgaven bidrage til en dybere forståelse af kryptografi og dets anvendelser i sikkerhedsdomænet og identificere muligheder og udfordringer ved implementeringen af kryptografiske teknikker i softwaremiljøer.


## Projektmål:

* TLS:<br>
Evaluere sikkerhedsaspekterne af TLS-protokollen i forhold til sikkerhedstrusler og angreb, og udvikle retningslinjer og bedste praksis til konfiguration og implementering af TLS i webbaserede applikationer.

* Praktisk implementering af kryptografiske algoritmer i software:<br>
Udvikle en bibliotek af kryptografiske funktioner, der omfatter kryptering, dekryptering og hashfunktionsoperationer, og demonstrer deres brug i forskellige softwareapplikationer med fokus på sikkerhed og effektivitet.

* RSA:<br>
Analyser implementeringsaspekterne af RSA-algoritmen og udvikle et praktisk system til sikker kommunikation baseret på RSA-kryptografi, inklusive nøglegenerering, kryptering og dekryptering.

* Steganografi:<br>
Udvikle og implementere en steganografisk metode til skjult kommunikation i digitale billeder og evaluere dens effektivitet og sikkerhed i forhold til deteksjon og skjulningskapacitet.