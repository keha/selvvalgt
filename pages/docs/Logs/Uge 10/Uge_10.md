# Uge 10

## Projekt status 
Stille stående...

## Ændringer i målene?
Hvad er målene?

## Vejledning
Jeg har i dag snakket med Martin bla. omkring hvorfor jeg ikke rigtig er kommet i gang, og hvordan jeg bare sidder "fast" når jeg prøver at komme i gang. Jeg synes at vi fik en god samtale ud af det med at sidde fast i "Hello-World-Helvedet" som Martin kaldet det. <br>
Han sagde også at jeg skulle passe på med ikke at gå i gang med for mange ting på én gang, så jeg ikke bliver for overvældet og går i baglås.
men at jeg skal tage det én ting af gangen og komme igang for tiden går hurtigt. <br>
Alt i alt har det været en meget god vejledning med en mere positiv udgang end jeg kom med.  

### Mål for de næste 14 dage
- Historie time? <-- Det er mest bare for mig selv.<br>
- Projektmål. <br>
- Læringsmål. <br>
- Problemformulering.

---

## Historie time

Kryptografiens historie kan spores tilbage til antikken, hvor gamle civilisationer som de egyptiske, romerske og græske kulturer anvendte forskellige former for kryptografi til at beskytte fortrolige beskeder. Et af de mest kendte eksempler er den skiftende Caesar-cipher, der blev brugt af Julius Cæsar til at kryptere militære meddelelser.

I middelalderen blev kryptografi stadig mere sofistikeret med udviklingen af metoder som Vigenère-chifferet, der tillod mere komplekse former for kryptering og dekryptering. På dette tidspunkt blev kryptografi ofte brugt i diplomatisk kommunikation og spionage.

Under renæssancen og den tidlige moderne tid opstod flere vigtige fremskridt inden for kryptografi, herunder opdagelsen af frekvensanalyse af Charles Babbage og udviklingen af polyalfabetiske chiffermetoder.

Det 20. århundrede så en markant stigning i kompleksiteten og anvendelsen af kryptografi, især under begge verdenskrige. Under Anden Verdenskrig spillede kryptografi en afgørende rolle i kampen mod koder og kryptografiske systemer, og der blev udviklet avancerede maskiner som Enigma-maskinen af tyskerne og kryptoanalytiske metoder som dem, der blev anvendt af Bletchley Park.

I det 20. og 21. århundrede har kryptografi været afgørende for udviklingen af internettet og digitale kommunikationssystemer. Moderne kryptografiske algoritmer som RSA, DES, AES og SHA har gjort det muligt at opnå sikker kommunikation, dataintegritet og autentifikation i en digital verden.

---

## Links
[History and Evolution of Cryptography and Cryptanalysis](https://www.youtube.com/watch?v=z9Qi5mDSYb8) <br>
[Cryptography - Wikipedia](https://en.wikipedia.org/wiki/Cryptography) <br>[History of Cryptography: Behind The Code - Episode 1](https://www.youtube.com/watch?v=U0m65tUkMj8) <br>
[History of Cryptography: Behind The Code - Episode 2](https://www.youtube.com/watch?v=j2WLzmd1ItQ) <br>
[History of Cryptography: Behind The Code - Episode 3](https://www.youtube.com/watch?v=MI5bTle8Ntk) <br>
[History of Cryptography: Behind The Code - Episode 4](https://www.youtube.com/watch?v=iXvZpM6Jjl4) <br>
[History of Cryptography: Behind The Code - Episode 5](https://www.youtube.com/watch?v=UnApvzz1IAw) <br>
