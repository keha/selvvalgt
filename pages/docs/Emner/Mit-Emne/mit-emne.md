# Kryptografi

Jeg har valgt at gå i dybden med kryptografi, da jeg synes at emnet er meget spændende, og super vigtigt.

### Hvad er Kryptografi?

Kryptografi handler om at beskytte information ved at omforme den til en form, der er vanskelig for uautoriserede personer at forstå. Det er en essentiel del af moderne informationssikkerhed, og det bruges over alt fra beskyttelse af personlig kommunikation til at sikrer finansielle transaktioner.

### Formål og Anvendelser

Formålet med kryptografi er at opnå fortrolighed, integritet, autenticitet af data og kommunikation. Det spiller en afgørende rolle i beskyttelsen af personlige oplysninger, kommunikation, digital identifikation og meget mere. Kryptografi anvendes i en lang række applikationer, herunder krypteret kommunikation, digitale signaturer, adgangskontrol og sikkerhedskopiering.

### Typer af Kryptografi

Der findes flere typer kryptografiske teknikker, herunder symmetrisk kryptografi, hvor den samme nøgle bruges til både kryptering og dekryptering af data, og asymmetrisk kryptografi, hvor separate nøgler anvendes til kryptering og dekryptering. Derudover omfatter moderne kryptografi også hashfunktioner, digitale signaturer og nøgleudvekslingsprotokoller.

### Nøgleord i Kryptografi

**Kryptering:** Dette er processen med at konvertere almindelig tekst eller data til en uforståelig form, kaldet kryptotekst, ved hjælp af en algoritme og en nøgle. Denne proces sikrer, at selvom nogen får adgang til kryptoteksten, kan de ikke forstå den uden den korrekte nøgle til at dekryptere den.

**Dekryptering:** Dette er modsætningen til kryptering. Det indebærer at konvertere kryptotekst tilbage til almindelig tekst ved hjælp af den korrekte dekrypteringsnøgle.

**Nøgler:** Nøgler er hemmelige værdier, der bruges i krypterings- og dekrypteringsprocessen. Der findes to hovedtyper af nøgler: symmetriske nøgler, hvor den samme nøgle bruges til både kryptering og dekryptering, og asymmetriske nøgler, hvor der bruges et sæt af to nøgler, en offentlig og en privat.

**Symmetrisk kryptografi:** I symmetrisk kryptografi bruges den samme nøgle til både kryptering og dekryptering. Det er en hurtig proces, men det kræver, at parterne på forhånd deler nøgler sikkert.

**Asymmetrisk kryptografi:** I asymmetrisk kryptografi bruges et sæt af to nøgler: en offentlig nøgle, der deles frit, og en privat nøgle, der holdes hemmelig. Data, der er krypteret med den offentlige nøgle, kan kun dekrypteres med den tilsvarende private nøgle og omvendt. Dette gør det lettere at distribuere nøgler sikkert.

**Digital signatur:** Kryptografi bruges også til at oprette digitale signaturer og kontrolsummeværdier, der kan bekræfte integriteten af en besked eller en fil. Dette gøres ved at anvende en hashfunktion, der omdanner data til en fast længde streng, kendt som en hæsh værdi, som derefter kan krypteres med en privat nøgle for at skabe en digital signatur.
