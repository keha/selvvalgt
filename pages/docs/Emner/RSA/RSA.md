# RSA
RSA er en kryptografisk algoritme, der bruges til sikker kommunikation og dataoverførsel over usikre netværk som f.eks. internettet. Algoritmen blev udviklet af Ron Rivest, Adi Shamir og Leonard Adleman i 1977 og er opkaldt efter deres efternavne. RSA er baseret på konceptet med asymmetrisk kryptering, hvilket betyder, at der bruges to separate nøgler til at kryptere og dekryptere data.

Den ene nøgle, kaldet den offentlige nøgle, bruges til kryptering af data, mens den anden nøgle, den private nøgle, bruges til dekryptering. Det unikke ved RSA er, at det er praktisk umuligt at beregne den private nøgle ud fra den offentlige nøgle, hvilket sikrer, at selvom den offentlige nøgle er kendt, forbliver kommunikationen sikker.

RSA-algoritmen er baseret på anvendelsen af to store primtal, normalt betegnet som p og q, og faktorisation af disse primtal. Sikkerheden af RSA afhænger af vanskeligheden ved at faktorisere meget store primtal, hvilket gør det til en af de mest anvendte kryptografiske algoritmer i verden i dag. RSA bruges i en lang række applikationer, herunder sikker e-mail-kommunikation, digitale signaturer, sikker adgangskontrol og sikker webbrowsing.


### PrimeFinder
Dette C#-program, finder og udskriver primtal inden for et angivet interval. 

**Main-metoden:** <br>
Starter programmet og beder brugeren om at indtaste start- og slutpunkterne for det interval, hvor primtal skal findes.
Kalder derefter FindPrimes-metoden for at finde og udskrive primtal i det angivne interval. <br>
**FindPrimes-metoden:** <br>
Modtager start- og slutpunkterne for intervallet som parametre.
Gennemgår alle tal i intervallet og kalder IsPrime-metoden for at tjekke, om hvert tal er et primtal.
Udskriver primtalene, der findes, til konsollen. <br>
**IsPrime-metoden:** <br>
Tager et tal som input og returnerer true, hvis det er et primtal, og false ellers.
Først tjekker den nogle simple tilfælde, hvor tallet ikke er et primtal (f.eks. hvis det er mindre eller lig med 1, eller hvis det er 2 eller 3).
Derefter implementerer den en mere avanceret primtalstest ved hjælp af 6k ± 1-mønsteret, som effektivt eliminerer behovet for at teste hver mulig divisor.
Returnerer true hvis tallet er et primtal, og false ellers.
Samlet set giver dette program en effektiv måde at finde og udskrive primtal inden for et brugerdefineret interval.

``` C#
using System;

class PrimeFinder
{
    static void Main()
    {
        // Prompt brugeren for intervallet
        Console.WriteLine("Indtast intervallet, hvor primtal skal findes:");
        Console.Write("Start: ");
        int start = int.Parse(Console.ReadLine());
        Console.Write("Slut: ");
        int end = int.Parse(Console.ReadLine());

        // Udskriv alle primtal i det specificerede interval
        Console.WriteLine("Primtal i intervallet [{0}, {1}]:", start, end);
        FindPrimes(start, end);
    }

    // Funktion til at finde primtal i et interval
    static void FindPrimes(int start, int end)
    {
        // Gennemgå alle tal i intervallet
        for (int number = start; number <= end; number++)
        {
            // Hvis tallet er et primtal, udskriv det
            if (IsPrime(number))
            {
                Console.WriteLine(number);
            }
        }
    }

    // Funktion til at tjekke om et tal er et primtal
    static bool IsPrime(int number)
    {
        // Tjekker for simple tilfælde, hvor tallet ikke er primtal
        if (number <= 1)
        {
            return false;
        }
        if (number == 2 || number == 3)
        {
            return true;
        }
        if (number % 2 == 0 || number % 3 == 0)
        {
            return false;
        }

        // Tjekker for primtal ved hjælp af 6k ± 1 mønsteret
        int divisor = 6;
        while (divisor * divisor - 2 * divisor + 1 <= number)
        {
            if (number % (divisor - 1) == 0)
            {
                return false;
            }
            if (number % (divisor + 1) == 0)
            {
                return false;
            }
            divisor += 6;
        }
        return true;
    }
}
```
Output: <br>
![prime output](../../images/Selvvalgt/Emner/RSA/Prime.png)

### RSA  algoritmen
RSA-algoritmen er baseret på principperne for modulær aritmetik, primtalsteori og brugen af eksponentiation. Her er en grundlæggende oversigt over den matematiske baggrund for RSA:

**1. Valg af nøgler:** <br>
To store primtal, 𝑝 og 𝑞, vælges. <br> 
Disse primtal multipliceres for at danne produktet 𝑛=𝑝𝑞, som bliver grundlaget for RSA's nøgler.

**2. Beregning af modulus:** <br>
Modulus, 𝑛, er produktet af de to primtal, 𝑝 og 𝑞. <br>
𝑛=𝑝𝑞

**3. Beregning af Euler's Totient-funktion (𝜙(𝑛)):** <br>
Euler's totient-funktion, 𝜙(𝑛), beregnes som produktet af (𝑝−1) og (𝑞−1), da 𝑝 og 𝑞 er primtal. 𝜙(𝑛)=(𝑝−1)(𝑞−1)

**4. Valg af den offentlige nøgle (𝑒):** <br>
En offentlig eksponent, 𝑒, vælges normalt som et tal, der er relativt primt med 𝜙(𝑛), typisk et mindre primtal, men det skal opfylde
1<𝑒<𝜙(𝑛) og gcd(𝑒,𝜙(𝑛))=1, hvor gcd står for "greatest common divisor" (største fælles divisor).

**5. Valg af den private nøgle (𝑑):** <br>
Den private eksponent, 𝑑, beregnes ved hjælp af den udvidede Euklidiske algoritme, hvor 𝑑 opfylder ligningen 𝑒𝑑≡1(mod 𝜙(𝑛)).

Den offentlige nøgle er derefter (𝑒,𝑛), og den private nøgle er (𝑑,𝑛).

Når en besked, 𝑀, skal krypteres til en kryptotekst, 𝐶, anvendes følgende formel: <br>
𝐶≡𝑀^𝑒(mod 𝑛)

Når kryptoteksten, 𝐶, skal dekrypteres tilbage til den oprindelige besked, 
𝑀, anvendes følgende formel: <br>
𝑀≡𝐶^𝑑(mod 𝑛)

Dette er den grundlæggende matematik bag RSA, der muliggør sikker kryptering og dekryptering af data ved hjælp af asymmetriske nøgler.


``` C#
using System;
using System.Numerics;
using System.Text;

class RSA
{
    static void Main()
    {
        // Generer nøgler
        BigInteger p = 9973; // Primtal 1
        BigInteger q = 8929; // Primtal 2
        BigInteger n = p * q; // Modulus
        BigInteger phi = (p - 1) * (q - 1); // Eulers totient funktion

        BigInteger e = 17; // Offentlig eksponent
        BigInteger d = ModInverse(e, phi); // Privat eksponent

        // Offentlig nøgle (e, n)
        Tuple<BigInteger, BigInteger> publicKey = Tuple.Create(e, n);
        // Privat nøgle (d, n)
        Tuple<BigInteger, BigInteger> privateKey = Tuple.Create(d, n);

        while (true)
        {
            Console.WriteLine("1. Krypter besked");
            Console.WriteLine("2. Dekrypter besked");
            Console.WriteLine("3. Afslut");

            Console.Write("Vælg en mulighed: ");
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    EncryptMessage(publicKey);
                    break;
                case "2":
                    DecryptMessage(privateKey);
                    break;
                case "3":
                    return;
                default:
                    Console.WriteLine("Ugyldigt valg. Prøv igen.");
                    break;
            }
        }
    }

    // RSA kryptering
    static void EncryptMessage(Tuple<BigInteger, BigInteger> publicKey)
    {
        Console.Write("Indtast besked til kryptering: ");
        string messageStr = Console.ReadLine();

        BigInteger e = publicKey.Item1;
        BigInteger n = publicKey.Item2;

        StringBuilder encryptedMessage = new StringBuilder();
        foreach (char c in messageStr)
        {
            BigInteger m = (BigInteger)c;
            BigInteger encryptedChar = BigInteger.ModPow(m, e, n);
            encryptedMessage.Append(encryptedChar.ToString() + " ");
        }

        Console.WriteLine("Krypteret besked: " + encryptedMessage.ToString().Trim());
    }

    // RSA dekryptering
    static void DecryptMessage(Tuple<BigInteger, BigInteger> privateKey)
    {
        Console.Write("Indtast krypteret besked: ");
        string encryptedMessage = Console.ReadLine();

        BigInteger d = privateKey.Item1;
        BigInteger n = privateKey.Item2;

        StringBuilder decryptedMessage = new StringBuilder();
        string[] encryptedChars = encryptedMessage.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        foreach (string encryptedChar in encryptedChars)
        {
            BigInteger c = BigInteger.Parse(encryptedChar);
            BigInteger decryptedChar = BigInteger.ModPow(c, d, n);
            decryptedMessage.Append((char)decryptedChar);
        }

        Console.WriteLine("Dekrypteret besked: " + decryptedMessage.ToString());
    }

    // Beregn multiplikativ invers modulo n
    static BigInteger ModInverse(BigInteger a, BigInteger n)
    {
        BigInteger i = n, v = 0, d = 1;
        while (a > 0)
        {
            BigInteger t = i / a, x = a;
            a = i % x;
            i = x;
            x = d;
            d = v - t * x;
            v = x;
        }
        v %= n;
        if (v < 0) v = (v + n) % n;
        return v;
    }
}

```
Output: <br>
![RSA output](../../images/Selvvalgt/Emner/RSA/RSA.png)