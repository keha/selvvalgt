# Caecar Cipher

Caesar Cipher, er en af de mest kendte og enkle former for krypteringsteknikker. Det er opkaldt efter Julius Caesar, der siges at have brugt det til at kommunikere med hans allierede.

Grundlæggende set erstatter Caesar Cipher hvert bogstav i beskeden med et bogstav, der er et fast antal pladser længere ned i alfabetet. For eksempel, med en forskydning på 5, ville bogstavet A blive erstattet med F, B med G, C med H og så videre.

Her er en hurtig demonstration:

Besked: "HELLO"
Forskydning: 5

H -> M <br>
E -> J <br>
L -> Q <br>
L -> Q <br>
O -> T <br>
Den krypterede besked ville være "MJQQT".

For at dekryptere en besked, gør du det modsatte, flytter hvert bogstav fem pladser til venstre.

Caesar Cipher er dog en meget svag form for kryptering, da der kun er 25 forskellige mulige forskydninger i det engelske alfabet (da en forskydning på 0 eller 26 ville give den oprindelige besked tilbage), og det er let at bryde ved simpel bruteforce-metode.


``` C#
using System;

class CaesarCipher
{
    static void Main(string[] args)
    {
        bool isRunning = true;

        // Kør programmet, indtil brugeren vælger at afslutte
        while (isRunning)
        {
            Console.WriteLine("Velkommen til Caesars Cipher program!");
            Console.WriteLine("Vælg en af følgende handlinger:");
            Console.WriteLine("1. Kryptere tekst");
            Console.WriteLine("2. Dekryptere tekst");
            Console.WriteLine("3. Afslut");

            // Læs brugerens valg
            Console.Write("Indtast dit valg: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    // Krypter tekst
                    Console.WriteLine("Indtast teksten, du vil kryptere:");
                    string plaintext = Console.ReadLine();

                    Console.WriteLine("Indtast skift: ");
                    int encryptShift = int.Parse(Console.ReadLine());

                    string encryptedText = Encrypt(plaintext, encryptShift);
                    Console.WriteLine($"Krypteret tekst: {encryptedText}");
                    break;

                case 2:
                    // Dekrypter tekst
                    Console.WriteLine("Indtast teksten, du vil dekryptere:");
                    string ciphertext = Console.ReadLine();

                    Console.WriteLine("Indtast skift: ");
                    int decryptShift = int.Parse(Console.ReadLine());

                    string decryptedText = Encrypt(ciphertext, -decryptShift);
                    Console.WriteLine($"Dekrypteret tekst: {decryptedText}");
                    break;

                case 3:
                    // Afslut programmet
                    isRunning = false;
                    Console.WriteLine("Programmet afsluttes.");
                    break;

                default:
                    // Håndter ugyldigt valg
                    Console.WriteLine("Ugyldigt valg. Prøv igen.");
                    break;
            }

            Console.WriteLine();
        }
    }

    // Metode til at kryptere tekst med Caesars Cipher
    static string Encrypt(string text, int shift)
    {
        string result = "";

        // Gennemgå hver karakter i teksten
        foreach (char ch in text)
        {
            // Kontroller om karakteren er en bogstav
            if (char.IsLetter(ch))
            {
                // Beregn den forskydning, og konverter karakteren
                char shiftedChar = (char)(ch + shift);

                // Håndter store bogstaver
                if (char.IsUpper(ch))
                {
                    if (shiftedChar > 'Z') // Hvis karakteren er gået ud over 'Z'
                        shiftedChar = (char)(ch - (26 - shift)); // Roter rundt til starten af alfabetet
                    else if (shiftedChar < 'A') // Hvis karakteren er gået ud under 'A'
                        shiftedChar = (char)(ch + (26 + shift)); ; // Roter rundt til slutningen af alfabetet
                }
                // Håndter små bogstaver
                else
                {
                    if (shiftedChar > 'z') // Hvis karakteren er gået ud over 'z'
                        shiftedChar = (char)(ch - (26 - shift)); // Roter rundt til starten af alfabetet
                    else if (shiftedChar < 'a') // Hvis karakteren er gået ud under 'a'
                        shiftedChar = (char)(ch + (26 + shift)); ; // Roter rundt til slutningen af alfabetet
                }
                // Tilføj den krypterede/dekrypterede karakter til resultatet
                result += shiftedChar;
            }
            else
            {
                // Hvis karakteren ikke er et bogstav, tilføj den uændret til resultatet
                result += ch;
            }
        }

        // Returnér den krypterede/dekrypterede tekst
        return result;
    }

}
```

Output: <br>
![Output](../../images/Selvvalgt/Emner/Caesar%20Cipher/Caesar Cipher.png)