# TLS

## TLS's anvendelse af kryptografi
TLS (Transport Layer Security) er en protokol, der anvendes til at sikre kommunikation over et netværk. TLS 1.2 & 1.3 er de nuværende netværksprotokol efter at SSL blev deprecated, men TLS bygger på SSL. TLS bliver brugt på transportlaget (lag 4) i OSI-modellen da det lag er ansvarlig for at etablere, styre og afslutte forbindelser og TLS sørger for at det er en sikker forbindelse hele vejen. TLS sørger for fortrolighed, integritet og autenticering af data, der transmitteres mellem to applikationer. TLS anvender forskellige kryptografiske metoder til at opnå disse sikkerhedsegenskaber. <br>

Her er nogle af de kryptografiske teknikker, som TLS anvender: <br>

**Symmetrisk kryptografi:** Ved hjælp af symmetriske krypteringsalgoritmer, såsom AES (Advanced Encryption Standard), krypteres data med en fælles nøgle, som både afsenderen og modtageren kender. Denne nøgle bruges til både at kryptere og dekryptere data under kommunikationen.

**Asymmetrisk kryptografi:** Også kendt som offentlig-nøgle-kryptografi, anvendes asymmetrisk kryptografi til at muliggøre autenticering og udveksling af en fælles nøgle til symmetrisk kryptering. TLS bruger normalt en asymmetrisk krypteringsalgoritme som RSA eller ECC (Elliptic Curve Cryptography) til at etablere en sikker forbindelse mellem klient og server, hvor de udveksler en fælles nøgle sikkert.

**Hashfunktioner:** TLS bruger hashfunktioner som SHA-256 til at sikre integriteten af dataene. Ved at oprette en hashværdi af dataene kan modtageren verificere, at de modtagne data ikke er blevet ændret under overførslen.

**Message Authentication Codes (MACs):** Disse bruges til at sikre, at meddelelser ikke er blevet ændret under overførslen. En MAC genereres ved hjælp af en kombination af en hemmelig nøgle og data, og den sendes sammen med dataene. Modtageren kan bruge den samme nøgle til at verificere, at dataene ikke er blevet ændret.

**Protokoller til håndtryk og nøgleudveksling:** TLS bruger protokoller som TLS Handshake-protokollen til at lade klienten og serveren forhandle om kryptografiske parametre og udveksle nøgler sikkert.

Samlet set anvender TLS en kombination af disse kryptografiske teknikker for at sikre, at data, der transmitteres over en netværksforbindelse, forbliver fortrolige, intakte og autentiske. Denne sikkerhed er afgørende for mange applikationer, der kræver beskyttelse af følsomme data. <br>


TLS håndtrykket er lidt anderlededes end TCP three-way-handshaket, men det er en essentiel del af oprettelsen af en sikker forbindelse mellem en klient og en server over internettet.  <br>
Det foregår typisk i fire faser:

**1. Indledende kontakt:** Klienten sender en forespørgsel til serveren for at etablere forbindelse. Denne forespørgsel indeholder de kryptografiske specifikationer, som klienten understøtter. <br>
**2. Serverens svar:** Serveren svarer tilbage med sin egen kryptografiske information, inklusive sit digitale certifikat, som indeholder serverens offentlige nøgle. <br>
**3. Godkendelse af certifikat:** Klienten validerer serverens certifikat. Dette indebærer typisk at kontrollere, om certifikatet er udstedt af en betroet myndighed (CA) og om det er udløbet eller blevet tilbagekaldt. <br>
**4. Nøgleudveksling og godkendelse:** Klienten genererer en sessionnøgle, krypterer den med serverens offentlige nøgle og sender den til serveren. Serveren dekrypterer nøglen med sin private nøgle. Nu kan begge parter generere en fælles nøgle til brug for kryptering og dekryptering af data under sessionen. <br>

Denne proces sikrer, at kommunikationen mellem klienten og serveren er fortrolig, autentificeret og integritetsbeskyttet, hvilket er afgørende for at beskytte mod angreb som f.eks. man-in-the-middle og aftapning af data.

## TLS Server
``` C#
using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        // Generer et selvsigneret X.509-certifikat.
        X509Certificate2 serverCertificate = GenerateSelfSignedCertificate("CN=localhost");

        // Start en TCP-listener.
        TcpListener listener = new TcpListener(IPAddress.Any, 4433);
        listener.Start();
        Console.WriteLine("Server started, listening on port 4433...");

        while (true)
        {
            // Lyt efter indkommende forbindelser.
            using (TcpClient client = listener.AcceptTcpClient())
            using (SslStream sslStream = new SslStream(client.GetStream(), false))
            {
                // Udfør SSL-håndtryk.
                sslStream.AuthenticateAsServer(serverCertificate, false, System.Security.Authentication.SslProtocols.Tls, true);
                // Udskriv detaljer om TLS-handshake
                Console.WriteLine("TLS Handshake:");
                Console.WriteLine("Protocol: " + sslStream.SslProtocol);
                Console.WriteLine("Cipher: " + sslStream.CipherAlgorithm + " " + sslStream.CipherStrength);
                Console.WriteLine("Hash: " + sslStream.HashAlgorithm + " " + sslStream.HashStrength);
                Console.WriteLine("Key exchange: " + sslStream.KeyExchangeAlgorithm + " " + sslStream.KeyExchangeStrength);
                Console.WriteLine("TLS handshake completed.");

                // Lyt efter beskeder fra klienten.
                while (true)
                {
                    byte[] buffer = new byte[1024];
                    int bytesRead = sslStream.Read(buffer, 0, buffer.Length);
                    if (bytesRead == 0)
                    {
                        Console.WriteLine("Connection closed by client.");
                        break;
                    }

                    string message = Encoding.UTF8.GetString(buffer, 0, bytesRead);
                    Console.WriteLine("Message received from client: " + message);

                    // Send svar til klienten.
                    string responseMessage = "Server received: " + message;
                    byte[] response = Encoding.UTF8.GetBytes(responseMessage);
                    sslStream.Write(response);
                    Console.WriteLine("Response sent to client: " + responseMessage);
                }
            }
        }
    }

    // Generer et selvsigneret X.509-certifikat.
    static X509Certificate2 GenerateSelfSignedCertificate(string subjectName)
    {
        using (RSA rsa = RSA.Create(2048))
        {
            var request = new CertificateRequest(subjectName, rsa, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            var certificate = request.CreateSelfSigned(DateTimeOffset.UtcNow, DateTimeOffset.UtcNow.AddYears(1));

            return new X509Certificate2(certificate.Export(X509ContentType.Pfx));
        }
    }
}

```
Output Server: <br>
![Output](../../images/Selvvalgt/Emner/TLS/TLS%20Server.png)
Her i outputtet fra serveren kan man se hvilken protokol, krypterings algoritme, hash funktion og den public nøgle der bliver brugt til denne forbindelse.

## TLS Klient
``` C#
using System;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        // Certifikatets navn (CN) - skal matche serverens certifikat.
        string certName = "localhost";

        // Opret en TCP-klient.
        using (TcpClient client = new TcpClient())
        {
            // Forbind til localhost på port 4433 (TLS).
            client.Connect("localhost", 4433);

            // Opret en SSL-stream.
            using (SslStream sslStream = new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateCertificate)))
            {
                // Udfør SSL-håndtryk.
                sslStream.AuthenticateAsClient(certName);
                Console.WriteLine("TLS handshake completed.");

                // Send besked til serveren.
                string message = "Hello from client!";
                byte[] messageBytes = Encoding.UTF8.GetBytes(message);
                sslStream.Write(messageBytes);
                Console.WriteLine("Message sent to server: " + message);

                // Modtag svar fra serveren.
                byte[] buffer = new byte[1024];
                int bytesRead = sslStream.Read(buffer, 0, buffer.Length);
                string response = Encoding.UTF8.GetString(buffer, 0, bytesRead);
                Console.WriteLine("Server response: " + response);
            }
        }
    }

    // Validerer serverens certifikat.
    static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        // Her kan du implementere tilpasset certifikatvalidering, hvis nødvendigt.
        // I dette eksempel accepterer vi alle certifikater (usikker tilstand - IKKE ANBEFALET til produktionsbrug).
        return true;
    }
}

```
Output Client: <br>
![Output](../../images/Selvvalgt/Emner/TLS/TLS%20Client.png)



## TLS Håndtryk med google
``` C#
using System.Net.Security;
using System.Net.Sockets;

class Program
{
    static void Main(string[] args)
    {
        // Opret en TCP-forbindelse til et TLS-kompatibelt værtsnavn og port
        TcpClient client = new TcpClient("google.com", 443);

        // Opret en SslStream til at håndtere TLS-håndtrykket
        using (SslStream sslStream = new SslStream(client.GetStream()))
        {
            // Start TLS-handshake
            sslStream.AuthenticateAsClient("google.com");

            // Udskriv detaljer om TLS-handshake
            Console.WriteLine("TLS Handshake:");
            Console.WriteLine("Protocol: " + sslStream.SslProtocol);
            Console.WriteLine("Cipher: " + sslStream.CipherAlgorithm + " " + sslStream.CipherStrength);
            Console.WriteLine("Hash: " + sslStream.HashAlgorithm + " " + sslStream.HashStrength);
            Console.WriteLine("Key exchange: " + sslStream.KeyExchangeAlgorithm + " " + sslStream.KeyExchangeStrength);
        }

        // Luk TCP-forbindelsen
        client.Close();
    }
}
```
Output Handshake til google: <br>
![Output](../../images/Selvvalgt/Emner/TLS/TLS%20Handshake.png)