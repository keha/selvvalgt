# Opsætning

Først naviger til det directory det pågældende projekt er clonet ned til.
lav et nyt directory kaldet "pages", og flyt "docs" og "mkdocs.yml" hertil.

Åben en terminal her og kør denne komando:
```
python3 -m venv env
```
Dette laver et nyt directory kaldet "env" i projektets directory, med et virtuelt enviroment som vi bruger til at køre gitlabpages lokalt.

Efter terminalen er færdig, skal det nye virtuelle enviroment startes, dette gøres men denne komando:
```
env\Scripts\activate
```

Altid sørg for at pip at updateret med denne komando:
```
python3 -m pip install --upgrade pip
```

Efter pip er updateret, brug denne komando til at hente alle depandencies:
```
python3 -m pip install requests
```

Herefter installer mkdocs med denne komando:
```
pip install mkdocs
```

Herefter burde det virke, naviger til directoriet "pages", og kør komandoen:
```
mkdocs serve
```



## Links

https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment

https://ucl-pba-its.gitlab.io/exercises/intro/3_intro_opgave_gitlab_pages/#instruktioner
