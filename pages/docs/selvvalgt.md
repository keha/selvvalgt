# Selvvalgt Fordybelse
Faget er på 5 ECTS point og er for 2. Semester studerende på PBA IT SIkkerhed.


5 ECTS point svarer til 135 timers arbejde, ud af de 135 timer er ca. 24% tilrettelagt med deltagelse af underviser, resten af tiden er uden underviser som studerende skal bruge på arbejde med opgaver og selvstudie.

Det forventes at den studerende tilstræber at arbejde ud fra niveau 6 (Bachelor niveau) i Kvalifikationsrammen for Livslang Læring

 
# Indhold

Målet med valgfaget selvvalgt fordybelsesområde er at give den studerende mulighed for at vælge og arbejde selvstændigt med et fordybelses område indenfor området IT Sikkerhed samt  at give den studerende mulighed for at træne selvstændigt projekt opstilling og eksekvering.

Den studerende opsætter selv læringsmål ud fra det valgte emne og indenfor rammerne i uddannelsens mål for læringsudbytte fra den nationale studieordning.

Den studerende sparre med underviser under hele forløbet, ved deltagelse i obligatoriske vejledningsmøder.

 

# Læringsmål for Selvvalgt fordybelse


## Viden
Efter fuldførelse af valgfaget har den studerende viden om:

* Egen læringsproces
* Opstilling af  læringsmål
* Opstilling af projektmål
* Et selvvalgt emne indenfor it-sikkerhed

## Færdigheder
Efter fuldførelse af valgfaget kan den studerende:

* Opstille egen læringsmål.
* Opstille projektmål
* Vurderer egen opfyldelse af lærings-og projektmål.

## Kompetencer
Efter fuldførelse af valgfaget kan den studerende:

* Selvstændigt tilegne sig ny viden og færdigheder indenfor  et nyt it sikkerheds emne.
* Selvstændigt opstille projekt og læringsmål.


### Eksamens beskrivelse
Eksamen er en skriftlig aflevering på wiseflow, bestående af en proces rapport samt et produkt i form af et offentlig tilgængeligt Gitlab projekt.

Proces rapporten skal som minimum indeholde følgende afsnit: <br>
- Indledning. <br>
- Overordnet beskrivelse af det valgte emne. <br>
- Beskrivelse af projektplan. <br>
- Beskrivelse af den overordnet tilgang til læringsprocessen af emnet. <br>
- Refleksioner over projektet samt læringsprocessen. <br>
- En tydelig reference til Gitlab projekt (produktet).

Herudover skal procesrapporten overholde de gængse krav for skriftlige afleveringer, beskrevet i uddannelsens studieordning.

Gitlab projekt skal som minimum indeholde følgende: <br>
- Dokumentation for projektet samt læring. <br>
- Udarbejdet eksempler samt evt. produkter (Infrastrukturs opsætninger, kode)